package com.quanquan.repository;

import org.springframework.data.repository.CrudRepository;

import com.quanquan.model.HangSua;

public interface HangSuaDAO extends CrudRepository<HangSua, String>{

}
