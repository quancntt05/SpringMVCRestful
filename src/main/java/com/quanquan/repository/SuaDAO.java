package com.quanquan.repository;


import org.springframework.data.repository.CrudRepository;

import com.quanquan.model.Sua;

public interface SuaDAO extends CrudRepository<Sua, String> {

}
