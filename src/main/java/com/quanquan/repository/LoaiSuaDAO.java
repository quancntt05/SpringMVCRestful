package com.quanquan.repository;

import org.springframework.data.repository.CrudRepository;

import com.quanquan.model.LoaiSua;

public interface LoaiSuaDAO extends CrudRepository<LoaiSua, String>{

}
