package com.quanquan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quanquan.model.Sua;
import com.quanquan.service.SuaService;

@RestController
public class ProductController {
	
	@Autowired
	private SuaService suaService;
	
	@RequestMapping(value = "/suas", method = RequestMethod.GET)
	public ResponseEntity<List<Sua>> index() {
		List<Sua> list = suaService.findAll();
		if (list.isEmpty()) {
			return new ResponseEntity<List<Sua>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Sua>>(list, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/suas", params="id",method = RequestMethod.GET)
	public ResponseEntity<Sua> getSuaById(@RequestParam String id) {
		Sua sua = suaService.findById(id);
		if (sua == null) {
			return new ResponseEntity<Sua>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Sua>(sua, HttpStatus.OK);
	}
}
