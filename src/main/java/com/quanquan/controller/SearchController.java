package com.quanquan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quanquan.model.HangSua;
import com.quanquan.model.LoaiSua;
import com.quanquan.model.Sua;
import com.quanquan.repository.HangSuaDAO;
import com.quanquan.repository.LoaiSuaDAO;
import com.quanquan.service.SuaService;

@RestController
public class SearchController {
	
	@Autowired
	private SuaService suaService;
	
	@Autowired
	private LoaiSuaDAO loaiSuaDAO;
	
	@Autowired
	private HangSuaDAO hangSuaDAO;
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ResponseEntity<?> getListByHangSuaLoaiSuaKey(
			@RequestParam("hangsua") String hangSuaKey,
			@RequestParam("loaisua") String loaiSuaKey,
			@RequestParam("key") String key) {
		
		HangSua hangSua = hangSuaDAO.findById(hangSuaKey).orElse(null);
		LoaiSua loaiSua = loaiSuaDAO.findById(loaiSuaKey).orElse(null);
		List<Sua> list = suaService.findingByLoaiSuaHangSuaKey(hangSua,loaiSua,key);
		if (list.isEmpty()) {
			return new ResponseEntity<List<Sua>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Sua>>(list, HttpStatus.OK);
	}
}
