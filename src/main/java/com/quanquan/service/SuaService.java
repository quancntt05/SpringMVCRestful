package com.quanquan.service;

import java.util.List;

import com.quanquan.model.HangSua;
import com.quanquan.model.LoaiSua;
import com.quanquan.model.Sua;

public interface SuaService {
	List<Sua> findAll();
	Sua findById(String id);
	List<Sua> findByHangSua(String hangSua);
	List<Sua> findByLoaiSua(String loaiSua);
	List<Sua> findingByLoaiSuaHangSuaKey(HangSua hangSua, LoaiSua loaiSua,String key);
	void deleteSua(String id);
}
