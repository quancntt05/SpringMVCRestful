package com.quanquan.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quanquan.model.HangSua;
import com.quanquan.model.LoaiSua;
import com.quanquan.model.Sua;
import com.quanquan.repository.SuaDAO;
import com.quanquan.utils.HibernateUtil;


@Service
@Transactional
public class SuaServiceImpl implements SuaService{
	
	@Autowired
	private SuaDAO suaDAO;

	@Override
	public List<Sua> findAll() {
		return (List<Sua>) suaDAO.findAll();
	}

	@Override
	public List<Sua> findByHangSua(String hangSua) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		Root<Sua> rootSua = query.from(Sua.class);
		Root<HangSua> rootHangSua = query.from(HangSua.class);
		
		Predicate sql1 = builder.equal(rootSua.get("hangSua"), rootHangSua.get("maHangSua"));
		Predicate sql2 = builder.equal(rootHangSua.get("maHangSua"), hangSua);
		
		query.select(rootSua)
			.where(builder.and(sql1,sql2));
		
		List<Sua> list = session.createQuery(query).getResultList();
		
		return list;
	}

	@Override
	public List<Sua> findByLoaiSua(String loaiSua) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		Root<Sua> rootSua = query.from(Sua.class);
		Root<LoaiSua> rootLoaiSua = query.from(LoaiSua.class);
		
		Predicate sql1 = builder.equal(rootSua.get("loaiSua"), rootLoaiSua.get("maLoaiSua"));
		Predicate sql2 = builder.equal(rootLoaiSua.get("maLoaiSua"), loaiSua);
		
		query.select(rootSua)
			.where(builder.and(sql1,sql2));
		
		List<Sua> list = session.createQuery(query).getResultList();
		
		return list;
	}

	@Override
	public List<Sua> findingByLoaiSuaHangSuaKey(HangSua hangSua, LoaiSua loaiSua, String key) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		
		Root<Sua> root = query.from(Sua.class);
		Predicate sql1 = builder.equal(root.get("loaiSua"), loaiSua);
		Predicate sql2 = builder.equal(root.get("hangSua"), hangSua);
		Predicate sql3 = builder.like(root.get("tenSua").as(String.class), "%"+key+"%");
		query.where(builder.and(sql1,sql2,sql3));

		List<Sua> list = session.createQuery(query).getResultList();
		return list;
	}

	@Override
	public void deleteSua(String id) {
		suaDAO.deleteById(id);
	}

	@Override
	public Sua findById(String id) {
			return suaDAO.findById(id).orElse(null);
	}


}
