package com.quanquan.utils;


import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {
	private static StandardServiceRegistry registry;
	private static SessionFactory factory;
	
	public static SessionFactory getSessionFactory() {
		if(factory == null) {
			registry = new StandardServiceRegistryBuilder()
					.configure("hibernate.cfg.xml")
					.build();
			
			MetadataSources metadataSources = new MetadataSources(registry);
			
			Metadata metadata = metadataSources.getMetadataBuilder().build();
			
			factory = metadata.getSessionFactoryBuilder().build();
			
		}
		return factory;
	}
	
	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
